#!/bin/bash

set -e

baseYear=2016
offset=2
currentDate=`date +"%Y-%m-%d_%T"`
currentMonth=`date +"%M"`
currentYear=`date +"%Y"`
destination_folder=/DATA/$currentYear/$currentMonth/$currentDate

echo $currentDate $currentMonth $currentYear

if [ $currentYear -gt $(($baseYear+$offset)) ];
then
   echo "test"
   rm -rf $(($currentYear-$offset-1))
   baseYear+=1
   echo $offset
fi

mkdir -p $currentYear/$currentMonth/$currentDate

cp -r /var/log/asterisk/* $destination_folder

echo "Execute successfully"
 
